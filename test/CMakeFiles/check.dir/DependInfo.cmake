# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/goddard/wtfkiller/test/main.cpp" "/home/goddard/wtfkiller/test/CMakeFiles/check.dir/main.cpp.o"
  "/home/goddard/wtfkiller/test/sanity_check.cpp" "/home/goddard/wtfkiller/test/CMakeFiles/check.dir/sanity_check.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "QT_NO_DEBUG"
  "QT_GUI_LIB"
  "QT_CORE_LIB"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
