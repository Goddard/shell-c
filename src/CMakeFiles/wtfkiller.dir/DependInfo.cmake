# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/goddard/wtfkiller/src/app.cpp" "/home/goddard/wtfkiller/src/CMakeFiles/wtfkiller.dir/app.cpp.o"
  "/home/goddard/wtfkiller/src/main.cpp" "/home/goddard/wtfkiller/src/CMakeFiles/wtfkiller.dir/main.cpp.o"
  "/home/goddard/wtfkiller/src/moc_app.cxx" "/home/goddard/wtfkiller/src/CMakeFiles/wtfkiller.dir/moc_app.cxx.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "QT_NO_DEBUG"
  "QT_GUI_LIB"
  "QT_CORE_LIB"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
