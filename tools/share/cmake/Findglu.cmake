INCLUDE (FindPackageHandleStandardArgs)
FIND_PACKAGE(PkgConfig ${glu_FIND_REQUIRED} ${glu_FIND_QUIETLY})
IF (PKG_CONFIG_FOUND)
    SET(PKG_CONFIG_PATH_ENV_VAR $ENV{PKG_CONFIG_PATH})
    IF (NOT PKG_CONFIG_PATH_ENV_VAR)
        IF (glu_FIND_REQUIRED)
        	MESSAGE (FATAL_ERROR "Environment variable PKG_CONFIG_PATH not set. Setting this variable is required in order for pkg-config to locate installed software packages.")
        ENDIF (glu_FIND_REQUIRED)
    ENDIF (NOT PKG_CONFIG_PATH_ENV_VAR)
    PKG_CHECK_MODULES (glu glu)
    IF (glu_FOUND)
        SET(glu_LIBRARY ${glu_LIBRARIES})
        SET(glu_INCLUDE_DIR ${glu_INCLUDEDIR})
        SET(glu_LIBRARY_DIR ${glu_LIBRARY_DIRS})
        IF (NOT glu_FIND_QUIETLY)
            MESSAGE(STATUS "    includedir: ${glu_INCLUDE_DIR}")
            MESSAGE(STATUS "    librarydir: ${glu_LIBRARY_DIR}")
        ENDIF (NOT glu_FIND_QUIETLY)
    ENDIF(glu_FOUND)
ENDIF (PKG_CONFIG_FOUND)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(glu DEFAULT_MSG glu_LIBRARY glu_INCLUDE_DIR)
